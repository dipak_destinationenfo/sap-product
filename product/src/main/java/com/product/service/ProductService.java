package com.product.service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.product.entity.Product;
import com.product.entity.Size;
import com.product.repository.ProductRepository;
import com.product.repository.SizeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
// Add service layer exception
@Service
@Transactional
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Cacheable(cacheNames="productCache", key="#product")
    public List<Product> getAllProduct(boolean brand, boolean price, boolean size, boolean color) {
        List<Product> products;
        if(brand) {
            products = getProductByBrand();
        } else if(price) {
            products = getProductByPrice();
        } else if(size) {
            products = getProductBySize();
        } else if(color) {
            products = getProductByColor();
        } else {
            products = new ArrayList<>(productRepository.findAll());
        }

        //Use mapper class to map the entity data to reponse object data
        return products;
    }

    public List<Product> getProductByPrice() {
        return new ArrayList<>(productRepository.groupByPrice());
    }

    public List<Product> getProductByColor() {
        return new ArrayList<>(productRepository.groupByColor());
    }

    public List<Product> getProductBySize() {
        return new ArrayList<>(productRepository.groupBySize());
    }

    public List<Product> getProductByBrand() {
        return new ArrayList<>(productRepository.groupByBrand());
    }
}
