package com.product.repository;

import com.product.entity.Product;
import com.product.entity.Size;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SizeRepository extends JpaRepository<Size, Long> {
}
