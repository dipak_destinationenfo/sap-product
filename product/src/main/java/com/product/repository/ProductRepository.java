package com.product.repository;

import java.util.List;

import com.product.entity.Product;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select new com.product.entity.Product(p.brand, p.color, p.price, p.size, p.type)"
            + "from Product p "
            + "group by p.brand.id")
    List<Product> groupByBrand();

    @Query("select new com.product.entity.Product(p.brand, p.color, p.price, p.size, p.type)"
            + "from Product p "
            + "group by p.size.id")
    List<Product> groupBySize();

    @Query("select new com.product.entity.Product(p.brand, p.color, p.price, p.size, p.type)"
            + "from Product p "
            + "group by p.type.id")
    List<Product> groupByType();

    @Query("select new com.product.entity.Product(p.brand, p.color, p.price, p.size, p.type)"
            + "from Product p "
            + "group by p.color.id")
    List<Product> groupByColor();

    @Query("select new com.product.entity.Product(p.brand, p.color, p.price, p.size, p.type)"
            + "from Product p "
            + "group by p.price")
    List<Product> groupByPrice();
}
