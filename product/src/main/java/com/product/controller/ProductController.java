package com.product.controller;

import javax.ws.rs.QueryParam;
import java.util.List;

import com.product.entity.Product;
import com.product.entity.Size;
import com.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/product")
    private List<Size> getAllEmployees(@QueryParam("brand") boolean brand,
                                       @QueryParam("price") boolean price,
                                       @QueryParam("size") boolean size,
                                       @QueryParam("color") boolean color) {
        return productService.getAllProduct(brand, price, size, color);
    }
}
