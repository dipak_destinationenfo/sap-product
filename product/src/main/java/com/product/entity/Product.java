package com.product.entity;

import javax.persistence.*;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Size size;
    @ManyToOne
    private Color color;
    @ManyToOne
    private Brand brand;
    @ManyToOne
    private Type type;
    private BigDecimal price;
}
